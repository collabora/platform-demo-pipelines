#  Copyright (C) 2019 Collabora Limited
#  Author: Guillaume Tucker <guillaume.tucker@collabora.com>
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to
#  deal in the Software without restriction, including without limitation the
#  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
#  sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
#  IN THE SOFTWARE.

import argparse
import string
import sys
import urllib.parse
import xmlrpc.client


def main(args):
    url = urllib.parse.urlparse(args.url)
    url = '{scheme}://{username}:{token}@{netloc}{path}'.format(
        scheme=url.scheme, username=args.user, token=args.token,
        netloc=url.netloc, path=url.path)
    connection = xmlrpc.client.ServerProxy(url)
    with open(args.job) as job:
        job_tpl = job.read()
    job = string.Template(job_tpl).substitute(
        name=args.name,
        kernel=args.kernel,
        ramdisk=args.ramdisk,
        base_url=args.base_url,
        resource_url=args.resource_url,
        callback=args.callback)
    connection.scheduler.submit_job(job)
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser("LAVA job submit tool")
    parser.add_argument("job",
                        help="Path to the job definition file to submit")
    parser.add_argument("--url", required=True,
                        help="LAVA API URL")
    parser.add_argument("--user", required=True,
                        help="LAVA API user name")
    parser.add_argument("--token", required=True,
                        help="LAVA API token")
    parser.add_argument("--name",
                        help="Job name")
    parser.add_argument("--kernel",
                        help="URL where to find the kernel binaries")
    parser.add_argument("--ramdisk",
                        help="URL where to find the ramdisk binary")
    parser.add_argument("--base-url",
                        help="URL where to find OS binary")
    parser.add_argument("--resource-url",
                        help="URL where to find test resources")
    parser.add_argument("--callback",
                        help="URL where to post the results callback")
    args = parser.parse_args()
    status = main(args)
    sys.exit(0 if status is True else 1)
