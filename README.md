Repository of jenkins pipelines to be used with [Platform Demo](https://gitlab.collabora.com/collabora/platform-demo-host)

# Pipelines

## Pipeline 1 - [qemu-debos.jpl](qemu-debos.jpl):
Steps:
* build plain debian rootfs with debos
* build kernel
* boot on qemu in LAVA
* send callback to Jenkins with LAVA job results (pass if login)

## Pipeline 2 - [kevin-panfrost.jpl](kevin-panfrost.jpl):
Steps:
* schedule fixed nfsroot boot on kevin with Panfrost
* run kmscube in LAVA
* run mpv in LAVA to play video using VPU and GPU

## Pipeline 3 - [odroid-igt.jpl](odroid-igt.jpl):
Steps:
* build kernel for arm
* boot stretch-igt with kernel on odroid-xu3
* run igt test suite
* send callback to jenkins with test results
